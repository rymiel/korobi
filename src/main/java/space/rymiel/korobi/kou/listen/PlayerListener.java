package space.rymiel.korobi.kou.listen;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import de.myzelyam.api.vanish.BungeeVanishAPI;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;
import space.rymiel.korobi.ChannelUtil;
import space.rymiel.korobi.kou.KouKorobi;

public class PlayerListener implements Listener {
  private final Logger parentLog = KouKorobi.getInstance().getLogger();

  @EventHandler(priority = EventPriority.LOWEST)
  public void onPostLogin(PostLoginEvent event) {
    // Triggered whenever a player joins
    final ProxiedPlayer player = event.getPlayer();
    if (player == null) {
      return;
    }

    scheduleCustomData(player, ChannelUtil.JOIN_PMEV);
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onPostDisconnect(PlayerDisconnectEvent event) {
    // Triggered whenever a player quits.
    final ProxiedPlayer player = event.getPlayer();
    if (player == null) {
      return;
    }

    if (player.getServer().getInfo().getPlayers().size() == 1) {
      // Last player to leave the server, can't contact it for the displayname
      String cachedName = nameFromCache(player.getUniqueId());
      if (cachedName == null) cachedName = player.getName();

      KouKorobi.getInstance()
          .getProxy()
          .broadcast(
              ChatColor.translateAlternateColorCodes(
                  '&',
                  KouKorobi.getQuitMessage()
                      .replaceAll("%cached_displayname%", cachedName)
                      .replaceAll("%(\\w.)%", "")) // HACK: very destructive regex
              );
      return;
    }

    // Vanish checks are done here to make sure to have a strong reference to the disconnecting
    // player before they're actually gone.
    if (KouKorobi.getInstance().getUsingVanish() && BungeeVanishAPI.isInvisible(player)) {
      // If the player is vanished, send a special message to everyone who can see vanished players.
      for (ProxiedPlayer p : KouKorobi.getInstance().getProxy().getPlayers()) {
        if (p.equals(player)) continue;
        if (p.hasPermission("pv.see"))
          p.sendMessage(
              ChatColor.translateAlternateColorCodes('&', KouKorobi.getVanishQuitSeeMessage())
                  .replaceAll("%player%", player.getName()));
      }
      return; // All other regular users will see nothing.
    }

    scheduleCustomData(player, ChannelUtil.QUIT_PMEV);
  }

  @EventHandler
  public void onMessage(PluginMessageEvent event) {
    if (!event.getTag().equalsIgnoreCase(ChannelUtil.PLUGIN_CHANNEL)) {
      return;
    }
    ByteArrayDataInput in = ByteStreams.newDataInput(event.getData());
    byte pmev = in.readByte();
    if (pmev == ChannelUtil.UPDATE_CACHE) {
      UUID player = UUID.fromString(in.readUTF());
      String display = in.readUTF();
      KouKorobi.getInstance()
          .debugMessage(String.format("caching %s = %s", player.toString(), display));
      KouKorobi.getInstance().getCache().put(player, display);
    } else if (pmev == ChannelUtil.VANISH_MIRROR && KouKorobi.getInstance().getUsingVanish()) {
      UUID player = UUID.fromString(in.readUTF());
      byte mirror = in.readByte();
      ProxiedPlayer proxyPlayer = KouKorobi.getInstance().getProxy().getPlayer(player);
      KouKorobi.getInstance()
          .debugMessage(
              String.format(
                  "~x mirroring '%c' for %s (%s) x~", mirror, player, proxyPlayer.getName()));
      scheduleCustomData(proxyPlayer, (char) mirror);
    }
  }

  public void sendCustomData(
      ProxiedPlayer player, Server sendTo, char pmevChannel, int tryNr, String... data) {
    Collection<ProxiedPlayer> networkPlayers = ProxyServer.getInstance().getPlayers();
    // Cancel if there are no players in the entire network
    if (networkPlayers == null || networkPlayers.isEmpty()) {
      KouKorobi.getInstance().getLogger().warning("no players, cancelling"); // just in case
    }

    if (sendTo == null) {
      sendTo = player.getServer();
    }

    // If the player has no associated server, schedule a retry to not run into a
    // NullPointerException.
    if (sendTo == null || sendTo.getInfo() == null) {
      // Retry increments the tryNr until it reaches the max
      if (tryNr + 1 > KouKorobi.getMaxRetries()) {
        parentLog.severe(
            String.format("GIVING UP ON SCHEDULING %s PMEV for %s", pmevChannel, player));
        return;
      }
      parentLog.warning(
          String.format(
              "Rescheduling %s PMEV for %s (try %s of %s)",
              pmevChannel, player, tryNr + 1, KouKorobi.getMaxRetries()));
      // Attempt to re-fetch the server of the player
      scheduleCustomData(player, player.getServer(), pmevChannel, tryNr + 1);
      return; // Don't even bother
    }

    ByteArrayDataOutput out = ByteStreams.newDataOutput();
    // Instead of a string for a subchannel, this system uses only a single byte called "PMEV".
    out.writeByte(pmevChannel);
    for (String e : data) out.writeUTF(e);
    byte[] outBytes = out.toByteArray();

    // Dispatch event to target player's server.
    sendTo.getInfo().sendData(ChannelUtil.PLUGIN_CHANNEL, outBytes);
  }

  private void scheduleCustomData(
      ProxiedPlayer player, Server sendTo, char pmevChannel, int tryNr) {
    Runnable scheduledRun = null;
    float scheduledDelay = 0.0f;
    float additionalDelay = KouKorobi.getRetryDelay() * tryNr * (tryNr / 2);

    switch (pmevChannel) {
      case ChannelUtil.JOIN_PMEV:
        scheduledRun = new JoinRunnable(player, sendTo, this, tryNr);
        scheduledDelay = KouKorobi.getJoinDelay();
        break;
      case ChannelUtil.QUIT_PMEV:
        scheduledRun = new QuitRunnable(player, sendTo, this, tryNr);
        scheduledDelay = KouKorobi.getQuitDelay();
        break;
      default:
        break;
        // Room for expansion...?
    }

    ProxyServer.getInstance()
        .getScheduler()
        .schedule(
            KouKorobi.getInstance().getPlugin(),
            scheduledRun,
            (long) ((scheduledDelay + additionalDelay) * 1000),
            TimeUnit.MILLISECONDS);
  }

  // Default case for tryNr = 0
  private void scheduleCustomData(ProxiedPlayer player, Server sendTo, char pmevChannel) {
    scheduleCustomData(player, sendTo, pmevChannel, 0);
  }

  private void scheduleCustomData(ProxiedPlayer player, char pmevChannel) {
    KouKorobi.getInstance()
        .debugMessage(
            String.format(
                "Automatically scheduling '%c' for %s using %s instead",
                pmevChannel, player.getName(), player.getServer()));
    scheduleCustomData(player, player.getServer(), pmevChannel, 0);
  }

  public String nameFromCache(UUID key) {
    try {
      return KouKorobi.getInstance().getCache().get(key);
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
    return null;
  }
}
