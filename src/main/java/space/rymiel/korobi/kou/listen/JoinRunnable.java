package space.rymiel.korobi.kou.listen;

import de.myzelyam.api.vanish.BungeeVanishAPI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import space.rymiel.korobi.ChannelUtil;
import space.rymiel.korobi.kou.KouKorobi;

public class JoinRunnable implements Runnable {
  private final ProxiedPlayer player;
  private final Server sendTo;
  private final PlayerListener caller;
  private final int tryNr;

  public JoinRunnable(ProxiedPlayer player, Server sendTo, PlayerListener caller, int tryNr) {
    this.player = player;
    this.caller = caller;
    this.tryNr = tryNr;
    this.sendTo = sendTo;
  }

  @Override
  public void run() {
    // Cancel the scheduled runnable if the player didn't actually connect or left instantly.
    if (player.isConnected()) {
      final String playerUUID = player.getUniqueId().toString();
      final String playerServer = player.getServer().getInfo().getName(); // TODO: can somehow error

      final boolean canBeFirstJoin =
          (playerServer.equals(KouKorobi.getInstance().getFirstJoinServer()));

      KouKorobi.getInstance()
          .debugMessage(
              String.format(
                  "?join <%s> ?target <%s> ?match <%s>",
                  playerServer, KouKorobi.getInstance().getFirstJoinServer(), canBeFirstJoin));
      KouKorobi.getInstance()
          .debugMessage(
              String.format(
                  ">> outgoing Join >> %s for %s in %s",
                  playerUUID, player, playerServer, canBeFirstJoin));

      if (KouKorobi.getInstance().getUsingVanish() && BungeeVanishAPI.isInvisible(player)) {
        // If the player is vanished, send a special message to everyone who can see vanished
        // players.
        for (ProxiedPlayer p : KouKorobi.getInstance().getProxy().getPlayers()) {
          if (p.equals(player)) continue;
          if (p.hasPermission("pv.see"))
            p.sendMessage(
                ChatColor.translateAlternateColorCodes('&', KouKorobi.getVanishJoinSeeMessage())
                    .replaceAll("%player%", player.getName()));
        }
        player.sendMessage(
            ChatColor.translateAlternateColorCodes('&', KouKorobi.getVanishJoinNotifyMessage())
                .replaceAll("%player%", player.getName()));
        return; // All other regular users will see nothing.
      }

      caller.sendCustomData(
          player,
          null, // Don't trust the event
          ChannelUtil.JOIN_PMEV,
          tryNr,
          playerUUID,
          KouKorobi.getJoinMessage(),
          canBeFirstJoin ? "y" : "n",
          KouKorobi.getFirstJoinMessage());
    }
  }
}
