package space.rymiel.korobi.kou.listen;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import space.rymiel.korobi.ChannelUtil;
import space.rymiel.korobi.kou.KouKorobi;

public class QuitRunnable implements Runnable {
  private final ProxiedPlayer player;
  private final Server sendTo;
  private final PlayerListener caller;
  private final int tryNr;

  public QuitRunnable(ProxiedPlayer player, Server sendTo, PlayerListener caller, int tryNr) {
    this.player = player;
    this.caller = caller;
    this.tryNr = tryNr;
    this.sendTo = sendTo;
  }

  // This quit handler probably doesn't need to be a schedule runnable but it keeps it symmetric.
  @Override
  public void run() {
    final String playerUUID = player.getUniqueId().toString();

    KouKorobi.getInstance().debugMessage(String.format(">> outgoing Quit >> %s", playerUUID));
    // Pass the test message to the bukkit side (backend)
    caller.sendCustomData(
        player, this.sendTo, ChannelUtil.QUIT_PMEV, tryNr, playerUUID, KouKorobi.getQuitMessage());
  }
}
