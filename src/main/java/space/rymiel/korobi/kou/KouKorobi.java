package space.rymiel.korobi.kou;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.NoSuchElementException;
import java.util.UUID;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import space.rymiel.korobi.ChannelUtil;
import space.rymiel.korobi.kou.listen.PlayerListener;

// This plugin goes on the bungee side and listens for player joins.
//
// KouKorobi (高転び), meaning "high fall" as the bungee server is "higher" in operating
// before passing info on to "lower" servers.
public final class KouKorobi extends Plugin {
  private static KouKorobi instance;
  private Plugin mainPlugin;
  private LoadingCache<UUID, String> cache;
  private static boolean debugMode = false;
  // Config vars
  // TODO: I don't use java enough to really know how to clean these up, so they're very repetitive.
  private static float joinDelay;
  private static float quitDelay;
  private static float retryDelay;
  private static int maxRetries;
  private static String joinMessage;
  private static String firstJoinMessage;
  private static String firstJoinServer;
  private static String vanishJoinSeeMessage;
  private static String vanishQuitSeeMessage;
  private static String vanishJoinNotifyMessage;
  private static String quitMessage;
  private static String quitDefaultPrefix;
  private static boolean usingVanish;

  @Override
  public void onEnable() {
    KouKorobi.setInstance(this);
    this.mainPlugin = this;
    getProxy().registerChannel(ChannelUtil.PLUGIN_CHANNEL);
    // The logic of listening for joining and leaving is in this PlayerListener
    getProxy().getPluginManager().registerListener(this, (Listener) new PlayerListener());

    if (!getDataFolder().exists()) getDataFolder().mkdir();

    File file = new File(getDataFolder(), "config.yml");

    if (!file.exists()) {
      try (InputStream in = getResourceAsStream("config.yml")) {
        Files.copy(in, file.toPath());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    try {
      Configuration cf = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
      // TODO: This is also very repetitive
      this.joinDelay               = cf.getFloat(  "delays.join",         3.0f);
      this.quitDelay               = cf.getFloat(  "delays.quit",         3.0f);
      this.retryDelay              = cf.getFloat(  "delays.retry",        3.0f);
      this.maxRetries              = cf.getInt(    "delays.max_retries",  3);
      this.joinMessage             = cf.getString( "join",                "Join message not configured");
      this.firstJoinMessage        = cf.getString( "first_join",          "First join message not configured");
      this.firstJoinServer         = cf.getString( "first_join_server",   "");
      this.vanishJoinSeeMessage    = cf.getString( "vanish.join.see",     "Vanish join see message not configured");
      this.vanishJoinNotifyMessage = cf.getString( "vanish.join.notify",  "Vanish join notify message not configured");
      this.quitMessage             = cf.getString( "quit",                "Quit message not configured");
      this.vanishQuitSeeMessage    = cf.getString( "vanish.quit.see",     "Vanish quit see message not configured");
      this.quitDefaultPrefix       = cf.getString( "quit_default_prefix", "");
      this.usingVanish             = cf.getBoolean("use_vanish",          false);
    } catch (IOException e) {
      e.printStackTrace();
    }

    if (getProxy().getPluginManager().getPlugin("PremiumVanish") != null && this.usingVanish) {
      getLogger().info("Using PremiumVanish API");
    } else {
      // TODO: this is actually untested
      this.usingVanish = false;
    }

    setupCache();
  }

  private void setupCache() {
    // A cache which stores player displaynames or gets their username.
    // Note that a cache MISS results in a plain username and not a displayname.
    LoadingCache<UUID, String> namecache =
        CacheBuilder.newBuilder()
            .maximumSize(100) // TODO: Make configurable?
            .build(
                new CacheLoader<UUID, String>() {
                  public String load(UUID key) throws NoSuchElementException {
                    ProxiedPlayer proxyPlayer = getProxy().getPlayer(key);
                    if (proxyPlayer == null)
                      throw new NoSuchElementException(
                          String.format("UUID %s isn't online", key.toString()));
                    String playerName = proxyPlayer.getName();
                    getLogger()
                        .warning(
                            String.format(
                                "|x| cache MISS |x| %s = %s (ProxyPlayer)",
                                key.toString(), playerName));
                    return playerName;
                  }
                });
    this.cache = namecache;
  }

  // Bad static methods, mostly repetitive
  // TODO: Reduce as much as possible
  public static KouKorobi getInstance() {
    return instance;
  }

  public static void setInstance(KouKorobi instance) {
    KouKorobi.instance = instance;
  }

  public Plugin getPlugin() {
    return this.mainPlugin;
  }

  public LoadingCache<UUID, String> getCache() {
    return this.cache;
  }

  public void debugMessage(String message) {
    if (debugMode) getLogger().info(String.format("[DEBUG] %s", message));
  }

  public static float getJoinDelay() {
    return joinDelay;
  }

  public static float getQuitDelay() {
    return quitDelay;
  }

  public static float getRetryDelay() {
    return retryDelay;
  }

  public static int getMaxRetries() {
    return maxRetries;
  }

  public static boolean getUsingVanish() {
    return usingVanish;
  }

  public static String getJoinMessage() {
    return joinMessage;
  }

  public static String getFirstJoinMessage() {
    return firstJoinMessage;
  }

  public static String getFirstJoinServer() {
    return firstJoinServer;
  }

  public static String getVanishJoinSeeMessage() {
    return vanishJoinSeeMessage;
  }

  public static String getVanishQuitSeeMessage() {
    return vanishQuitSeeMessage;
  }

  public static String getVanishJoinNotifyMessage() {
    return vanishJoinNotifyMessage;
  }

  public static String getQuitMessage() {
    return quitMessage;
  }

  public static String getQuitDefaultPrefix() {
    return quitDefaultPrefix;
  }
}
