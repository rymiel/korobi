package space.rymiel.korobi;

public final class ChannelUtil {

  public static final String PLUGIN_CHANNEL = "korobi:msg";
  public static final String SPREAD_CHANNEL = "korobi:spread";
  public static final char JOIN_PMEV = 'j';
  public static final char UPDATE_CACHE = 'b';
  public static final char QUIT_PMEV = 'q';
  public static final char SPREAD_PMEV = 's';
  public static final char VANISH_MIRROR = 'v';

  // Helper class, cannot be instantiated.
  private ChannelUtil() {
    throw new AssertionError();
  }
}
