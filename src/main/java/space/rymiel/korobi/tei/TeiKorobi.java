package space.rymiel.korobi.tei;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import space.rymiel.korobi.ChannelUtil;
import space.rymiel.korobi.tei.listen.DisplayNameTask;
import space.rymiel.korobi.tei.listen.QuitListener;
import space.rymiel.korobi.tei.listen.VanishListener;

// This plugin goes on the bukkit side and listens for messages from KouKorobi.
//
// TeiKorobi (低転び), derived from KouKorobi and meaning "low fall" as it receives info
// from the "higher" bungee server.
public final class TeiKorobi extends JavaPlugin implements PluginMessageListener {
  private LoadingCache<UUID, String> cache;
  private static TeiKorobi instance;
  private static boolean debugMode = false;

  @Override
  public void onEnable() {
    if (!checkIfBungee()) {
      return;
    }

    TeiKorobi.setInstance(this);

    getServer().getMessenger().registerOutgoingPluginChannel(this, ChannelUtil.PLUGIN_CHANNEL);
    getServer()
        .getMessenger()
        .registerIncomingPluginChannel(this, ChannelUtil.PLUGIN_CHANNEL, this);
    getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
    getServer().getPluginManager().registerEvents(new QuitListener(), this);
    getServer().getPluginManager().registerEvents(new VanishListener(), this);

    setupCache();

    long delay = 1200L;
    final DisplayNameTask task = new DisplayNameTask();
    task.runTaskTimer(this, delay, delay);
  }

  @Override
  public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
    ByteArrayDataInput in = ByteStreams.newDataInput(bytes);
    if (channel.equalsIgnoreCase(ChannelUtil.PLUGIN_CHANNEL)) {
      // Direct message from KouKorobi: A player has joined or quit the network on this very server.
      byte pmev = in.readByte();
      String spreadMessage = null;

      if (pmev != ChannelUtil.JOIN_PMEV && pmev != ChannelUtil.QUIT_PMEV)
        return; // Unsupported PMEV

      // Join message structure:
      // (byte)'j' (UTF)uuid (UTF)template (UTF)canBeFirstJoin (UTF)firstJoinTemplate
      // canBeFirstJoin can only be "y" or "n"

      // Quit message structure:
      // (byte)'q' (UTF)uuid (UTF)template
      String data = in.readUTF();
      UUID dataKey = UUID.fromString(data);
      String template = in.readUTF();

      if (pmev == ChannelUtil.JOIN_PMEV) {
        String canBeFirstJoinString = in.readUTF();
        boolean canBeFirstJoin = (canBeFirstJoinString.equalsIgnoreCase("y"));
        String firstJoinTemplate = in.readUTF();
        Player newPlayer = Bukkit.getPlayer(dataKey);
        boolean firstJoin = (canBeFirstJoin && !newPlayer.hasPlayedBefore());

        debugMessage(
            String.format(
                "<< incoming Join << %s %s (first join %s, %s; has played %s)",
                data,
                newPlayer.getName(),
                canBeFirstJoinString,
                firstJoin,
                newPlayer.hasPlayedBefore()));
        if (firstJoin) {
          spreadMessage = PlaceholderAPI.setPlaceholders(newPlayer, firstJoinTemplate);
        } else {
          spreadMessage = PlaceholderAPI.setPlaceholders(newPlayer, template);
        }
      } else {
        String leftPlayerName = this.nameFromCache(dataKey);
        OfflinePlayer leftPlayer = Bukkit.getOfflinePlayer(dataKey);

        debugMessage(String.format("<< incoming Quit << %s %s", data, leftPlayerName));
        spreadMessage =
            PlaceholderAPI.setPlaceholders(
                leftPlayer, template.replaceAll("%cached_displayname%", leftPlayerName));
      }
      getServer().broadcastMessage(spreadMessage); // Brodcast the message locally

      if (spreadMessage != null) {
        // Forward the same message to all other bungeed servers, aka "spread" it to others.
        ByteArrayDataOutput spreadOut = ByteStreams.newDataOutput();
        spreadOut.writeUTF("Forward");
        spreadOut.writeUTF("ONLINE");
        spreadOut.writeUTF(ChannelUtil.SPREAD_CHANNEL);

        // BungeeCord Forward messages encapsulate another message within it.
        ByteArrayDataOutput spreadOutBody = ByteStreams.newDataOutput();
        spreadOutBody.writeByte(ChannelUtil.SPREAD_PMEV);
        spreadOutBody.writeUTF(spreadMessage);

        spreadOut.writeShort(spreadOutBody.toByteArray().length);
        spreadOut.write(spreadOutBody.toByteArray());

        player.sendPluginMessage(this, "BungeeCord", spreadOut.toByteArray());
        debugMessage(String.format(">> outgoing Spread >> %s", spreadMessage));
      }

    } else if (channel.equalsIgnoreCase("BungeeCord")) {
      // Forwarded message from another TeiKorobi, no further processing by this server.
      String subChannel = in.readUTF();
      // These messages come from a subchannel from the SPREAD_CHANNEL constant but also use
      // a PMEV which is redundant, but expandable.
      if (!subChannel.equalsIgnoreCase(ChannelUtil.SPREAD_CHANNEL)) return;

      short len = in.readShort();
      byte[] msgbytes = new byte[len];
      in.readFully(msgbytes);

      // Spread encapsulated message structure:
      // (byte)pmev (UTF)message; where pmev is 's'.
      ByteArrayDataInput msgin = ByteStreams.newDataInput(msgbytes);
      msgin.readByte(); // not really necessary
      String forwardedMessage = msgin.readUTF(); // Read the data in the same way you wrote it
      debugMessage(String.format("<< incoming Spread << %s", forwardedMessage));

      getServer().broadcastMessage(forwardedMessage);
    }
  }

  public void updateBungeeCache(Player player) {
    UUID key = player.getUniqueId();
    String rememberName = player.getDisplayName();
    ByteArrayDataOutput out = ByteStreams.newDataOutput();
    out.writeByte(ChannelUtil.UPDATE_CACHE);
    out.writeUTF(key.toString());
    out.writeUTF(rememberName);

    player.sendPluginMessage(this, ChannelUtil.PLUGIN_CHANNEL, out.toByteArray());
    getCache().put(player.getUniqueId(), player.getDisplayName());
    debugMessage(String.format(">> outgoing Cache Update >> %s = %s", key, rememberName));
  }

  public void vanishMirror(Player player, char mirrorPmev) {
    UUID key = player.getUniqueId();
    ByteArrayDataOutput out = ByteStreams.newDataOutput();
    out.writeByte(ChannelUtil.VANISH_MIRROR);
    out.writeUTF(key.toString());
    out.writeByte(mirrorPmev);

    player.sendPluginMessage(this, ChannelUtil.PLUGIN_CHANNEL, out.toByteArray());
    debugMessage(
        String.format(
            ">> outgoing Vanish Mirror >> '%s' %s (%s)", mirrorPmev, key, player.getName()));
  }

  private boolean checkIfBungee() {
    // Avoid exposing plugin channels when not protected by bungee.
    if (!getServer()
        .spigot()
        .getConfig()
        .getConfigurationSection("settings")
        .getBoolean("bungeecord")) {
      this.getLogger().severe("This server is not BungeeCord. Disabling...");
      return false;
    }
    return true;
  }

  private void setupCache() {
    // A cache which stores player displaynames or gets their OfflinePlayer username.
    // Note that a cache MISS results in a plain username and not a displayname.
    LoadingCache<UUID, String> namecache =
        CacheBuilder.newBuilder()
            .maximumSize(100) // TODO: Make configurable?
            .build(
                new CacheLoader<UUID, String>() {
                  public String load(UUID key) throws NoSuchElementException {
                    OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(key);
                    if (offlinePlayer == null)
                      throw new NoSuchElementException(
                          String.format("UUID %s hasn't been on this server", key.toString()));
                    String offlineName = offlinePlayer.getName();
                    TeiKorobi.getInstance()
                        .getLogger()
                        .warning(
                            String.format(
                                "|x| cache MISS |x| %s = %s (OfflinePlayer)",
                                key.toString(), offlineName));
                    return offlineName;
                  }
                });
    this.cache = namecache;
  }

  public LoadingCache<UUID, String> getCache() {
    return this.cache;
  }

  public static TeiKorobi getInstance() {
    return instance;
  }

  public static void setInstance(TeiKorobi instance) {
    TeiKorobi.instance = instance;
  }

  public void debugMessage(String message) {
    if (debugMode) getLogger().info(String.format("[DEBUG] %s", message));
  }

  // Helper function for cache with a default case.
  public String nameFromCache(UUID key) {
    try {
      return getCache().get(key);
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
    return "&4Ghost";
  }
}
