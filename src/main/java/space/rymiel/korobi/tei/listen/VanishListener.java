package space.rymiel.korobi.tei.listen;

import de.myzelyam.api.vanish.PlayerHideEvent;
import de.myzelyam.api.vanish.PlayerShowEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import space.rymiel.korobi.ChannelUtil;
import space.rymiel.korobi.tei.TeiKorobi;

public final class VanishListener implements Listener {
  @EventHandler(priority = EventPriority.MONITOR)
  public void onVanish(PlayerHideEvent e) {
    Player p = e.getPlayer();
    if (e.isSilent()) return;
    TeiKorobi.getInstance().vanishMirror(p, ChannelUtil.QUIT_PMEV);
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onUnvanish(PlayerShowEvent e) {
    Player p = e.getPlayer();
    if (e.isSilent()) return;
    TeiKorobi.getInstance().vanishMirror(p, ChannelUtil.JOIN_PMEV);
  }
}
