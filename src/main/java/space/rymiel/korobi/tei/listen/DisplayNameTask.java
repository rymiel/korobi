package space.rymiel.korobi.tei.listen;

import com.google.common.cache.LoadingCache;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import space.rymiel.korobi.tei.TeiKorobi;

public class DisplayNameTask extends BukkitRunnable {
  @Override
  public void run() {
    TeiKorobi.getInstance().debugMessage("~~ Running DisplayNameTask ~~");
    final LoadingCache<UUID, String> cache = TeiKorobi.getInstance().getCache();
    for (Player p : Bukkit.getOnlinePlayers()) {
      String cachedName = null;
      try {
        cachedName = cache.get(p.getUniqueId());
      } catch (ExecutionException e) {
        e.printStackTrace();
        continue;
      }
      String realName = p.getDisplayName();
      TeiKorobi.getInstance()
          .debugMessage(
              String.format(
                  "~| (real) %s; (cached) %s (%s)",
                  realName, cachedName, p.getUniqueId().toString()));
      if (!cachedName.equals(realName)) {
        TeiKorobi.getInstance()
            .getLogger()
            .info(
                String.format(
                    "|~| cache UPDATE |~| '%s' replaced by '%s' (DisplayNameTask)",
                    cachedName, realName));
        TeiKorobi.getInstance().updateBungeeCache(p);
      }
    }
  }
}
