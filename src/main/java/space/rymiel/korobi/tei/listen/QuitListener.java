package space.rymiel.korobi.tei.listen;

import java.util.UUID;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import space.rymiel.korobi.tei.TeiKorobi;

public final class QuitListener implements Listener {
  // When a player leaves a server, cache their displayname which will then be looked up when it is
  // determined
  // the player left the entire network.
  @EventHandler(priority = EventPriority.LOWEST)
  public void onPlayerQuit(PlayerQuitEvent event) {
    Player player = event.getPlayer();
    UUID key = player.getUniqueId();
    String rememberName = player.getDisplayName();
    TeiKorobi.getInstance()
        .debugMessage(
            String.format(
                "|>| caching |<| %s = %s (PlayerQuitEvent)", key.toString(), rememberName));
    TeiKorobi.getInstance().getCache().put(key, rememberName);
  }
}
