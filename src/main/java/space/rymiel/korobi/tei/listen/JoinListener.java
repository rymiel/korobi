package space.rymiel.korobi.tei.listen;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import space.rymiel.korobi.tei.TeiKorobi;

public final class JoinListener implements Listener {
  // When a player joins the server, send their displayname to KouKorobi for the bungee cache.
  @EventHandler(priority = EventPriority.MONITOR)
  public void onPlayerJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();
    TeiKorobi.getInstance()
        .getServer()
        .getScheduler()
        .scheduleSyncDelayedTask(
            TeiKorobi.getInstance(),
            new Runnable() {
              @Override
              public void run() {
                TeiKorobi.getInstance().updateBungeeCache(player);
              }
            },
            60L);
  }
}
