# Korobi  
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/222db07ff129459e91556dcbe66913cb)](https://www.codacy.com/gl/rymiel/korobi/dashboard)

Minecraft plugin to send global join and leave message over a bungee network
with the player's display name.

## Why?

Display names tend to be handled by plugins local to a specific server which
cannot know on their own if a player joining it has just opened a new connection
or merely switched servers through bungee.

You'd have to make a trade-off, choosing between having fancy names that include
your players' rank, or global messages which don't spam the chat every time
someone visits a different server in your network.

With Korobi, you can have both. Also, why not.

## How?

Korobi consists of two separated instances, KouKorobi and TeiKorobi.

KouKorobi is a singular authoritative instance running on the bungee proxy
itself whereas TeiKorobi is an instance running on every single separate server
connected to this bungee.

When a player joins the network, KouKorobi issues a plugin message to the
specific server the player just joined to construct the join message itself.
That server broadcasts the join locally, and then forwards it on to all other
TeiKorobi listeners, which will also broadcast it on their respective servers.

The result is every server getting the same join message.

When a player leaves the network, KouKorobi issues a similar plugin message
to the server from which the player just left, however, because of delays the
specific server, it won't have a reference to the player to fetch their display
name. For this, whenever a player leaves *any* server (which includes switching
servers within the bungee network), the server caches the player's last known
display name, which will be fetched when KouKorobi confirms the player has
left the network, not just this server.

## Usage

Clone the repository with `git clone` and build the plugin with
`mvn clean package`. The resulting jar file goes in all the backend servers
*and* the bungee proxy. A config file will be generated in the bungee plugins
folder and none of the backend servers need a config, meaning it only needs
to be managed from a single place.
